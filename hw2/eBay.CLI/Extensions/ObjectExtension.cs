﻿using System.Collections.Generic;
using System.Reflection;

namespace eBay.CLI.Extensions
{
    public static class ObjectExtension
    {
        public static IEnumerable<string> GetAllPublicProperties(this object obj)
        {
            List<string> result = new();

            foreach (PropertyInfo p in obj.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance))
            {
                result.Add(p.Name);
            }

            return result;
        }

        public static IEnumerable<string> GetPropertyValues(this object obj, IEnumerable<string> propertyNames)
        {
            List<string> result = new();

            foreach (var name in propertyNames)
            {
                result.Add(obj.GetType().GetProperty(name).GetValue(obj)?.ToString() ?? "NULL");
            }

            return result;
        }
    }
}
