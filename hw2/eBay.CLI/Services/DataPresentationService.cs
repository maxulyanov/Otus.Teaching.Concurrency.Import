﻿using ConsoleTables;
using eBay.CLI.Helpers;
using eBay.Infrastructure.Map;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;

namespace eBay.CLI.Services
{
    public interface IDataPresentationService
    {
        void ShowUsersTable();
        void ShowItemsTable();
        void ShowBetsTable();

        void ShowHelp();
    }
    class DataPresentationService : ConsoleHelper, IDataPresentationService
    {
        readonly IDataContext _session;
        readonly ILogger<DataPresentationService> _logger;

        public DataPresentationService(IDataContext session, ILogger<DataPresentationService> logger)
        {
            _session = session;
            _logger = logger;
        }

        public void ShowBetsTable()
        {
            try
            {
                ConsolePrintWithBackColor(ConsoleColor.DarkGreen, "\r\nТаблица ставок:\r\n");
                var rows = _session.BetRepository.Get().Select(s => new
                {
                    s.Id,
                    ItemId = s.Item.Id,
                    OwnerId = s.Owner.Id,
                    Bet = s.BetValue.ToString("0.00"),
                    Date = s.Created.ToString("yyyy-MM-dd HH:mm:ss")
                }).ToList();
                if (rows?.Count > 0)
                {
                    ConsoleTable
                        .From(rows)
                        .Configure(o => o.NumberAlignment = Alignment.Right)
                        .Write(Format.Default);
                }
                else
                {
                    Console.WriteLine("Нет данных в таблице ставок");
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "ShowBetsTable");
            }
        }

        public void ShowItemsTable()
        {
            try
            {
                ConsolePrintWithBackColor(ConsoleColor.DarkGreen, "\r\nТаблица лотов:\r\n");
                var rows = _session.ItemRepository.Get().Select(s => new
                {
                    s.Id,
                    OwnerId = s.Owner.Id,
                    s.Name,
                    Description = $"{s.Description.Substring(0, Math.Min(50, s.Description.Length))}...",
                    s.CurrentValue
                }).ToList();
                if (rows?.Count > 0)
                {
                    ConsoleTable
                        .From(rows)
                        .Configure(o => o.NumberAlignment = Alignment.Right)
                        .Write(Format.Default);
                }
                else
                {
                    Console.WriteLine("Нет данных в таблице лотов");
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "ShowItemsTable");
            }
        }

        public void ShowUsersTable()
        {
            try
            {
                ConsolePrintWithBackColor(ConsoleColor.DarkGreen, "\r\nТаблица пользователей:\r\n");
                var rows = _session.UserRepository.Get().Select(s => new { s.Id, s.Name, s.Email }).ToList();
                if (rows?.Count > 0)
                {
                    ConsoleTable
                        .From(rows)
                        .Configure(o => o.NumberAlignment = Alignment.Right)
                        .Write(Format.Default);
                }
                else
                {
                    Console.WriteLine("Нет данных в таблице пользователей");
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "ShowUsersTable");
            }
        }

        public void ShowHelp()
        {
            Console.Clear();
            ConsolePrintWithForeColor(ConsoleColor.Green, "Вывод табличной информации:");
            ConsolePrintWithForeColor(ConsoleColor.DarkGreen, " - список пользователей: 1");
            ConsolePrintWithForeColor(ConsoleColor.DarkGreen, " - список лотов: 2");
            ConsolePrintWithForeColor(ConsoleColor.DarkGreen, " - список ставок: 3");

            ConsolePrintWithForeColor(ConsoleColor.Magenta, "Добавление:");
            ConsolePrintWithForeColor(ConsoleColor.DarkMagenta, " - добавить пользователя: 4");
            ConsolePrintWithForeColor(ConsoleColor.DarkMagenta, " - добавить лот: 5");
            ConsolePrintWithForeColor(ConsoleColor.DarkMagenta, " - добавить ставку: 6");

            ConsolePrintWithForeColor(ConsoleColor.DarkYellow, "Выход: CTRL+C\r\n");
            Console.Write("Выберите опцию: ");
        }
    }
}
