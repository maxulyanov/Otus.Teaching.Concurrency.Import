﻿using eBay.CLI.Helpers;
using eBay.Domain.AggregatesModel;
using eBay.Infrastructure.Map;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace eBay.CLI.Services
{
    public interface IDataWriterService
    {
        Task AddNewUser();
        Task AddNewBet();
        Task AddNewItem();
    }

    class DataWriterService : ConsoleHelper, IDataWriterService
    {
        readonly IDataContext _session;
        readonly ILogger<DataPresentationService> _logger;

        public DataWriterService(IDataContext session, ILogger<DataPresentationService> logger)
        {
            _session = session;
            _logger = logger;
        }

        private bool TryAgain(string infoMessage)
        {
            Console.Write($"{infoMessage}\r\nПопытаетесь ещё раз? (y/n): ");
            bool result = Console.ReadKey().KeyChar.ToString().ToLower() == "y";
            Console.WriteLine();
            return result;
        }

        public async Task AddNewBet()
        {
            ConsolePrintWithBackColor(ConsoleColor.Magenta, "\r\nДобавление новой ставки\r\n");
            again:
            Console.Write("Id владельца ставки: ");
            string ownerStr = Console.ReadLine();
            Console.Write("Id лота: ");
            string itemStr = Console.ReadLine();

            if (!int.TryParse(ownerStr, out int ownerId) |
                !int.TryParse(itemStr, out int itemId))
            {
                if (TryAgain("Некорректный данные, ожидаются целые числа для Id и #.## для ставки."))
                {
                    goto again;
                }

                return;
            }

            _session.BeginTransaction();
            try
            {
                User owner = _session.UserRepository.Find(ownerId).Result;
                Item item = _session.ItemRepository.Find(itemId).Result;

                if (owner == null || item == null)
                {
                    if (TryAgain("Некорректные идентификаторы, указанного пользователя или лота не существует."))
                    {
                        goto again;
                    }

                    return;
                }

                newBet:
                Console.Write($"Ставка (>{item.CurrentValue}): ");
                string valueStr = Console.ReadLine();
                if (!decimal.TryParse(valueStr, System.Globalization.NumberStyles.AllowDecimalPoint, System.Globalization.CultureInfo.InvariantCulture, out decimal value) ||
                    value <= item.CurrentValue)
                {
                    if (TryAgain($"Некорректная ставка, ставка должна быть выше чем {item.CurrentValue}."))
                    {
                        goto newBet;
                    }

                    return;
                }

                Bet bet = Bet.Create(0, owner, item, value);
                item.CurrentValue = value;

                await _session.BetRepository.InsertOrUpdate(bet);
                await _session.ItemRepository.InsertOrUpdate(item);
                await _session.Commit();
                ConsolePrintWithForeColor(ConsoleColor.DarkGreen, $"Ставка '{value}' добавлена пользователем '{owner.Name}' для лота '{item.Name}', id={bet.Id}");
            }
            catch (Exception ex)
            {
                await _session.Rollback();
                _logger.LogError(ex, "AddNewBet");
            }
            finally
            {
                _session.CloseTransaction();
            }
        }

        public async Task AddNewItem()
        {
            ConsolePrintWithBackColor(ConsoleColor.Magenta, "\r\nДобавление нового лота\r\n");
            again:
            Console.Write("Id владельца лота: ");
            string ownerId = Console.ReadLine();
            if (!int.TryParse(ownerId, out int Id))
            {
                if (TryAgain("Некорректный идентификатор."))
                {
                    goto again;
                }

                return;
            }

            User owner = _session.UserRepository.Find(Id).Result;
            if (owner == null)
            {
                if (TryAgain($"Пользователя с id={Id} - не существует"))
                {
                    goto again;
                }

                return;
            }

            Console.Write("Название лота: ");
            string itemName = Console.ReadLine();
            Console.Write("Описание лота: ");
            string itemDescription = Console.ReadLine();
            Item item = Item.Create(0, itemName, itemDescription, 1.0m, DateTime.UtcNow.AddDays(30), owner);
            _session.BeginTransaction();
            try
            {
                await _session.ItemRepository.InsertOrUpdate(item);
                await _session.Commit();
                ConsolePrintWithForeColor(ConsoleColor.DarkGreen, $"Лот '{item.Name}' добавлен, id={item.Id}");
            }
            catch (Exception ex)
            {
                await _session.Rollback();
                _logger.LogError(ex, "AddNewItem");
            }
            finally
            {
                _session.CloseTransaction();
            }
        }

        public async Task AddNewUser()
        {
            ConsolePrintWithBackColor(ConsoleColor.Magenta, "\r\nДобавление нового пользователя\r\n");
            again:
            Console.Write("Имя пользователя: ");
            string userName = Console.ReadLine();
            if (string.IsNullOrEmpty(userName))
            {
                if (TryAgain("Некорректное имя пользователя."))
                {
                    goto again;
                }

                return;
            }

            Console.Write("Email пользователя: ");
            string userEmail = Console.ReadLine();

            _session.BeginTransaction();
            try
            {
                User u = User.Create(0, userName, userEmail);
                await _session.UserRepository.InsertOrUpdate(u);
                await _session.Commit();
                ConsolePrintWithForeColor(ConsoleColor.DarkGreen, $"Пользователь '{u.Name}' добавлен, id={u.Id}");
            }
            catch (Exception ex)
            {
                await _session.Rollback();
                _logger.LogError(ex, "AddNewUser");
            }
            finally
            {
                _session.CloseTransaction();
            }
        }
    }
}
