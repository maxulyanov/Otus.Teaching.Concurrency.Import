﻿using System;

namespace eBay.CLI.Helpers
{
    class ConsoleHelper
    {
        readonly ConsoleColor _defBackColor = Console.BackgroundColor;
        readonly ConsoleColor _defForeColor = Console.ForegroundColor;

        public void ConsolePrintWithBackColor(ConsoleColor backColor, string text)
        {
            Console.BackgroundColor = backColor;
            Console.WriteLine(text);
            Console.BackgroundColor = _defBackColor;
        }

        public void ConsolePrintWithForeColor(ConsoleColor foreColor, string text)
        {
            Console.ForegroundColor = foreColor;
            Console.WriteLine(text);
            Console.ForegroundColor = _defForeColor;
        }
    }
}
