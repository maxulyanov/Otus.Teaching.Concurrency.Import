﻿using eBay.Domain.Common;
using System;
using System.Collections.Generic;

namespace eBay.Domain.AggregatesModel
{
    public interface IBetRepository : IRepository<Bet>, IDisposable
    {
    }
}
