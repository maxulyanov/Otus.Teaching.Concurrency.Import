﻿using eBay.Domain.Common;
using System;
using System.Collections.Generic;

namespace eBay.Domain.AggregatesModel
{
    /// <summary>
    /// Сущность пользователя
    /// </summary>
    public class User : Entity
    {
        /// <summary>
        /// Имя пользователя
        /// </summary>
        public virtual string Name { get; set; }
        /// <summary>
        /// Адресс пользователя
        /// </summary>
        public virtual string Email { get; set; }
        /// <summary>
        /// Список позиций пользователя
        /// </summary>
        public virtual IEnumerable<Item> Items { get; set; }
        /// <summary>
        /// Список ставок пользователя
        /// </summary>
        public virtual IEnumerable<Bet> Bets { get; set; }

        /// <summary>
        /// Метод создания объекта пользователя
        /// </summary>
        /// <param name="id">Идентификатор</param>
        /// <param name="name">Имя</param>
        /// <param name="email">Адрес</param>
        /// <returns></returns>
        public static User Create(int id, string name, string email)
        {
            if (string.IsNullOrEmpty(name))
                throw new ArgumentException("User.Create: Значение не может быть пустым", nameof(name));

            var user = new User
            {
                Id = id,
                Email = email?.Trim(),
                Name = name.Trim()
            };

            return user;
        }
    }
}
