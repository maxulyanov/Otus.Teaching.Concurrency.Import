using eBay.Domain.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eBay.Domain.AggregatesModel
{
    public interface IItemRepository : IRepository<Item>, IDisposable
    {
    }
}
