﻿using System;
using System.Threading.Tasks;

namespace eBay.Domain.Common
{
    public interface IUnitOfWork : IDisposable
    {
        void BeginTransaction();
        Task Commit();
        Task Rollback();
        void CloseTransaction();
    }
}
