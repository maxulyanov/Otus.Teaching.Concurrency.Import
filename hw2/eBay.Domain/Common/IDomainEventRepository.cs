﻿using MediatR;
using System.Collections.Generic;

namespace eBay.Domain.Common
{
    public interface IDomainEventRepository
    {
        /// <summary>
        /// Список событий сущности
        /// </summary>
        List<INotification> DomainEvents { get; }

        /// <summary>
        /// Метод подписывает сущность на событие
        /// </summary>
        /// <param name="eventItem"></param>
        void AddDomainEvent(INotification eventItem);

        /// <summary>
        /// Метод "отписки" от события для сущности
        /// </summary>
        /// <param name="eventItem"></param>
        void RemoveDomainEvent(INotification eventItem);
    }
}
