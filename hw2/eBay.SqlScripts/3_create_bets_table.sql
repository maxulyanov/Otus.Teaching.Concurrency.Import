﻿-- Table: public.bets

-- DROP TABLE IF EXISTS public.bets;

CREATE TABLE IF NOT EXISTS public.bets
(
    id integer NOT NULL,
    betvalue numeric(19,5),
    userid integer,
    itemid integer,
    created timestamp without time zone NOT NULL,
    CONSTRAINT bets_pkey PRIMARY KEY (id),
    CONSTRAINT fk_5910d7eb FOREIGN KEY (itemid)
        REFERENCES public.items (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fk_97fd18df FOREIGN KEY (userid)
        REFERENCES public.users (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.bets
    OWNER to ebay;