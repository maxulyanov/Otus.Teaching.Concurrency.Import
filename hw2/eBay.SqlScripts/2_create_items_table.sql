﻿-- Table: public.items

-- DROP TABLE IF EXISTS public.items;

CREATE TABLE IF NOT EXISTS public.items
(
    id integer NOT NULL,
    name character varying(100) COLLATE pg_catalog."default" NOT NULL,
    description character varying(300) COLLATE pg_catalog."default",
    currentvalue numeric(19,5),
    closedateutc timestamp without time zone NOT NULL,
    userid integer,
    itemid integer,
    CONSTRAINT items_pkey PRIMARY KEY (id),
    CONSTRAINT items_name_key UNIQUE (name),
    CONSTRAINT fk_9bf69e0e FOREIGN KEY (itemid)
        REFERENCES public.users (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fk_fb8c4be7 FOREIGN KEY (userid)
        REFERENCES public.users (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.items
    OWNER to ebay;