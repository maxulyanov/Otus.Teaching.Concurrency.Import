﻿using eBay.Domain.AggregatesModel;
using NHibernate;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;

namespace eBay.Infrastructure.Map
{
    public class ItemMap : ClassMapping<Item>
    {
        /// <summary>
        /// Маппинг данных сущности <seealso cref="Item"/>
        /// </summary>
        public ItemMap()
        {
            Table("Items");

            Id(item => item.Id, map =>
                {
                    map.Generator(Generators.SequenceIdentity);
                    map.Type(NHibernateUtil.Int32);
                    map.UnsavedValue(0);
                });

            Property(item => item.Name, map =>
            {
                map.Type(NHibernateUtil.String);
                map.Length(100);
                map.NotNullable(true);
                map.Unique(true);
            });

            Property(item => item.Description, map =>
            {
                map.Type(NHibernateUtil.String);
                map.Length(300);
            });

            Property(item => item.CloseDateUtc, map =>
            {
                map.Type(NHibernateUtil.DateTime);
                map.NotNullable(true);
            });

            Property(item => item.CurrentValue, map => map.Type(NHibernateUtil.Decimal));

            Bag(item => item.Bets, map => map.Key(k => k.Column("ItemId")), rel => rel.OneToMany());

            ManyToOne(item => item.Owner, map => map.Column("UserId"));    
            
        }
    }
}
