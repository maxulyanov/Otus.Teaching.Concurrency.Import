﻿namespace SolidPrincipals.Models
{
    /// <summary>
    /// Модель данных настроек игры "Угадай число"
    /// </summary>
    public class GuessTheNumber
    {
        public int MaxNumber { get; set; }
        public int MinNumber { get; set; }
        public int TryCount { get; set; }

        public override string ToString()
        {
            return $"MinNumber:{MinNumber}, MaxNumber:{MaxNumber}, TryCount:{TryCount}";
        }
    }
}
