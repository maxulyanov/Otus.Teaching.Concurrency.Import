﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Serilog;
using SolidPrincipals.Games;
using SolidPrincipals.Services;

class Program
{
    static IConfiguration Configuration { get; } = new ConfigurationBuilder()
                    .SetBasePath(AppDomain.CurrentDomain.BaseDirectory)
                    .AddJsonFile("appsettings.json", false, true)
                    .AddEnvironmentVariables().Build();

    static void Main(string[] args)
    {
        AppDomain.CurrentDomain.UnhandledException += (s, e) => { Log.Error(((Exception)e.ExceptionObject).Message); };

        Environment.CurrentDirectory = AppDomain.CurrentDomain.BaseDirectory;
        Log.Logger = new LoggerConfiguration().ReadFrom.Configuration(Configuration).CreateLogger();
        Log.Information("SolidPrincipals app is run");

        CreateHostBuilder(args).Build().Run();
    }

    public static IHostBuilder CreateHostBuilder(string[] args)
    {
        var host = Host.CreateDefaultBuilder(args)
            .UseSerilog()
            .ConfigureServices((context, services) =>
            {
                services.AddTransient<IGameWriter, ConsoleWriter>();
                services.AddTransient<IGameReader, ConsoleReader>();
                services.AddHostedService<GameService>();
            });

        return host;
    }
}