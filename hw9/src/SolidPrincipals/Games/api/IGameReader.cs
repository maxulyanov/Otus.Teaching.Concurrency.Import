﻿namespace SolidPrincipals.Games
{
    /// <summary>
    /// Интерфейс методов чтения данных их источника ввода
    /// </summary>
    public interface IGameReader
    {
        /// <summary>
        /// Метод чтения строки
        /// </summary>
        /// <returns></returns>
        string ReadString();

        /// <summary>
        /// Метод чтения целого числа
        /// </summary>
        /// <exception cref="FormatException" />
        /// <exception cref="OverflowException" />
        /// <returns></returns>
        int ReadInt();

        /// <summary>
        /// Метод подтверждение (Y/N)
        /// </summary>
        /// <returns>возвращает true если введён Y(y) иначе false</returns>
        bool ConfirmationDialog();
    }
}
