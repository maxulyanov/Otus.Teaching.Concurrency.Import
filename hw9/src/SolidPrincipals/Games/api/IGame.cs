﻿
namespace SolidPrincipals.Games
{
    /// <summary>
    /// Базовый интерфейс игры
    /// </summary>
    public interface IGame
    {
        /// <summary>
        /// Название игры
        /// </summary>
        string Name { get; }

        /// <summary>
        /// Метод вывода информации перед стартом (например правила)
        /// </summary>
        void Intro();

        /// <summary>
        /// Метод запускает игру
        /// </summary>
        void RunGame();

        /// <summary>
        /// Метод останавливает игру
        /// </summary>
        void StopGame();

        /// <summary>
        /// Метод вывода информации после окончания игры (например: итоговый счёт, результат)
        /// </summary>
        void Epilogue();
    }
}
