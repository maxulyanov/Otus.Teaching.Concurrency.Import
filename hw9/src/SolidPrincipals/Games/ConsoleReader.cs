﻿namespace SolidPrincipals.Games
{
    /// <summary>
    /// Класс чтения игровых данных из консоли
    /// </summary>
    internal class ConsoleReader : IGameReader
    {
        public bool ConfirmationDialog()
        {
            return Console.ReadKey().KeyChar.ToString().ToLower() == "y";
        }

        public int ReadInt()
        {
            return int.Parse(ReadString());
        }

        public string ReadString()
        {
            return Console.ReadLine() ?? string.Empty;
        }


    }
}
