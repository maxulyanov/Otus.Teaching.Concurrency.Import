﻿namespace SolidPrincipals.Games
{
    /// <summary>
    /// Класс методов вывода игровых данных в консоль
    /// </summary>
    internal class ConsoleWriter : IGameWriter
    {
        readonly ConsoleColor _defBackColor = ConsoleColor.Black;
        readonly ConsoleColor _defForeColor = ConsoleColor.White;

        public void PrintHeadline(string text)
        {
            Console.BackgroundColor = ConsoleColor.DarkGreen;
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine();
            Console.Write(text);
            Console.WriteLine();
            Console.BackgroundColor = _defBackColor;
            Console.ForegroundColor = _defForeColor;
        }

        public void PrintSuccess(string text)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write(text);
            Console.ForegroundColor = _defForeColor;
        }

        public void PrintError(string text)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.Write(text);
            Console.ForegroundColor = _defForeColor;
        }

        public void PrintStandart(string text)
        {
            Console.ForegroundColor = _defForeColor;
            Console.Write(text);
        }

        public void PrintQuestion(string text)
        {
            Console.ForegroundColor = ConsoleColor.DarkYellow;
            Console.Write(text);
            Console.ForegroundColor = _defForeColor;
        }

        public void EraseAllData()
        {
            Console.Clear();
        }
    }
}
