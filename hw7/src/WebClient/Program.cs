﻿using Bogus;
using System;
using WebClient.Services;

namespace WebClient
{
    class Program
    {
        public static void Main(string[] args)
        {
            try
            {
                // базовый адрес сервера передаётся первым параметром иначе используем дефолтный
                string baseUri = args?.Length > 0 ? args[0] : "http://localhost:5000";                
                ConsoleHttpActionService actions = new ConsoleHttpActionService(baseUri);               

                do
                {
                    actions.ShowHelp();
                    switch (Console.ReadLine())
                    {
                        case "1":
                            actions.ClientGetAction();
                            break;
                        case "2":
                            actions.ClientPostAction();
                            break;
                        default:
                            continue;
                    }
                } while (actions.TryAgain("Вернуться в меню выбора действия?"));
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}