﻿using Bogus;
using ConsoleTables;
using System;
using System.Collections.Generic;
using System.Net.Http.Json;
using System.Text.Json;
using WebClient.DTO;
using WebClient.Helpers;
using WebClient.Models;

namespace WebClient.Services
{
    class ConsoleHttpActionService : ConsoleHelper
    {
        readonly HttpRequest _httpRequest;
        readonly TimeSpan _httpTimeOut;

        readonly string _customerGetAction = "/customers/{0}";
        readonly string _customerPostAction = "/customers";

        public ConsoleHttpActionService(string serverUri)
        {
            _httpRequest = new HttpRequest(serverUri);
            _httpTimeOut = TimeSpan.FromSeconds(120);
        }

        public bool TryAgain(string infoMessage)
        {
            Console.Write($"{infoMessage} (y/n): ");
            bool result = Console.ReadKey().KeyChar.ToString().ToLower() == "y";
            Console.WriteLine();
            return result;
        }

        private void CustomerByIdRequest(long id)
        {
            try
            {
                var json = _httpRequest.Get(string.Format(_customerGetAction, id), _httpTimeOut).Result;

                if (_httpRequest.LastStatusCode == 200)
                {
                    ConsolePrintWithForeColor(ConsoleColor.Blue, $"HTTP_RESPONSE_CODE: {_httpRequest.LastStatusCode}\r\n");
                    var customer = JsonSerializer.Deserialize<Customer>(json);
                    ConsoleTable
                        .From(new List<Customer> { customer })
                        .Configure(o => o.NumberAlignment = Alignment.Right)
                        .Write(Format.Minimal);
                }
                else
                {
                    ConsolePrintWithForeColor(ConsoleColor.Red, $"HTTP_ERROR_RESPONSE_CODE: {_httpRequest.LastStatusCode}");
                }
            }
            catch (Exception ex)
            {
                ConsolePrintWithForeColor(ConsoleColor.Red, $"ERROR: {ex.Message}");
            }
        }

        private string CustomerGenerationPost(CustomerCreateRequest customerRequest)
        {
            try
            {
                var customerId = _httpRequest.Post(_customerPostAction, JsonContent.Create(customerRequest), _httpTimeOut).Result;

                if (_httpRequest.LastStatusCode == 200)
                {
                    ConsolePrintWithForeColor(ConsoleColor.Blue, $"HTTP_RESPONSE_CODE: {_httpRequest.LastStatusCode}\r\n");
                    return customerId;
                }
                else
                {
                    ConsolePrintWithForeColor(ConsoleColor.Red, $"HTTP_ERROR_RESPONSE_CODE: {_httpRequest.LastStatusCode}");
                }
            }
            catch (Exception ex)
            {
                ConsolePrintWithForeColor(ConsoleColor.Red, $"ERROR: {ex.Message}");
            }

            return string.Empty;
        }

        public void ClientGetAction()
        {
            ConsolePrintWithBackColor(ConsoleColor.DarkGreen, "\r\nДействие: получить клиента по ID\r\n");
            again:
            Console.Write("Id клиента: ");
            string customerStr = Console.ReadLine();

            if (!long.TryParse(customerStr, out long id))
            {
                if (TryAgain("Некорректный данные, ожидаются целые числа для ID. Повторить ввод?"))
                {
                    goto again;
                }

                return;
            }

            CustomerByIdRequest(id);
        }

        public void ClientPostAction()
        {
            ConsolePrintWithBackColor(ConsoleColor.DarkGreen, "\r\nДействие: генерация клиента и отправка на сервер\r\n");
            CustomerCreateRequest customer = RandomCustomer();
            string response = CustomerGenerationPost(customer);

            if (long.TryParse(response, out long id))
            {
                CustomerByIdRequest(id);
            }
            else
            {
                ConsolePrintWithForeColor(ConsoleColor.Red, $"Некорректный ответ сервера, ожидается целое число: '{response}'");
            }
        }

        public void ShowHelp()
        {
            Console.Clear();
            ConsolePrintWithForeColor(ConsoleColor.Blue, $"Установлен сервер данных: {_httpRequest.BaseUri}");
            ConsolePrintWithForeColor(ConsoleColor.Green, "Список действий:");
            ConsolePrintWithForeColor(ConsoleColor.DarkGreen, " - получить клиента по ID: 1");
            ConsolePrintWithForeColor(ConsoleColor.DarkGreen, " - сгенерировать клиента и отправить на сервер: 2");

            ConsolePrintWithForeColor(ConsoleColor.DarkYellow, "Выход: CTRL+C\r\n");
            Console.Write("Выберите опцию: ");
        }

        private static CustomerCreateRequest RandomCustomer()
        {
            Faker faker = new();
            return new CustomerCreateRequest(faker.Person.FirstName, faker.Person.LastName);
        }
    }
}
