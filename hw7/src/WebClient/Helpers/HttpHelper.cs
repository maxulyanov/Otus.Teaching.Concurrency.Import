﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace WebClient.Helpers
{

    /// <summary>
    /// Класс методов для работы с Web запросами
    /// </summary>
    public class HttpRequest
    {
        readonly Uri _baseAddress;
        public HttpRequest(string baseAddress)
        {
            _baseAddress = new Uri(baseAddress);
        }

        public string BaseUri => _baseAddress.AbsoluteUri;

        private Dictionary<string, string> pHeaders { get; set; }

        /// <summary>
        /// Код ответа последнего запроса
        /// </summary>
        public int LastStatusCode { get; private set; }

        /// <summary>
        /// Очистить заголовки Http запроса
        /// </summary>
        public void ClearHeaders()
        {
            if (pHeaders != null)
            {
                pHeaders.Clear();
            }
        }

        /// <summary>
        /// Добавить заголовок Http запроса
        /// </summary>
        /// <param name="Key"></param>
        /// <param name="Value"></param>
        public void AddHeader(string Key, string Value)
        {
            if (pHeaders == null)
            {
                pHeaders = new Dictionary<string, string>();
            }

            pHeaders.Add(Key, Value);
        }

        /// <summary>
        /// Отправка данных на внешний ресурс
        /// </summary>
        /// <param name="requestUri">Строка запроса</param>
        /// <param name="content">Данные для отправки</param>
        /// <param name="timeOut">Время ожидания ответа</param>
        /// <exception cref="TimeoutException"></exception>
        /// <exception cref="OperationCanceledException"></exception>
        /// <exception cref="HttpRequestException"></exception>
        public async Task<string> Post(string requestUri, HttpContent content, TimeSpan timeOut)
        {
            using (var client = new HttpClient())
            {
                client.Timeout = timeOut;
                client.BaseAddress = _baseAddress;

                if (pHeaders?.Count > 0)
                {
                    foreach (var h in pHeaders)
                    {
                        client.DefaultRequestHeaders.Add(h.Key, h.Value);
                    }
                }

                HttpResponseMessage pResponse = await client.PostAsync(requestUri, content);

                if (pResponse != null)
                {
                    LastStatusCode = (int)pResponse.StatusCode;

                    if (pResponse.Content != null)
                    {
                        return await pResponse.Content.ReadAsStringAsync();
                    }
                }
            }

            return string.Empty;
        }

        /// <summary>
        /// Получение данных с внешнего ресурса
        /// </summary>
        /// <param name="RequestUri">Строка запроса</param>
        /// <exception cref="OperationCanceledException"></exception>
        /// <exception cref="HttpRequestException"></exception>
        public async Task<string> Get(string requestUri, TimeSpan timeOut)
        {
            using (var client = new HttpClient())
            {
                client.Timeout = timeOut;
                client.BaseAddress = _baseAddress;

                if (pHeaders?.Count > 0)
                {
                    foreach (var h in pHeaders)
                    {
                        client.DefaultRequestHeaders.Add(h.Key, h.Value);
                    }
                }

                HttpResponseMessage pResponse = await client.GetAsync(requestUri);

                if (pResponse != null)
                {
                    LastStatusCode = (int)pResponse.StatusCode;

                    if (pResponse.Content != null)
                    {
                        return await pResponse.Content.ReadAsStringAsync();
                    }
                }
            }

            return string.Empty;
        }
    }
}
