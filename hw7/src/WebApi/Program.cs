using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using WebApi.Models;

namespace WebApi
{
    public static class Program
    {
        public static bool InsideIIS() => Environment.GetEnvironmentVariable("APP_POOL_ID") is string;
        public static string OS => RuntimeInformation.OSDescription;

        public static async Task Main(string[] args)
        {
            await CreateHostBuilder(args).Build().RunAsync();
        }

        private static IHostBuilder CreateHostBuilder(string[] args) 
        {
            var host = Host.CreateDefaultBuilder(args)
                .ConfigureServices((context,services) => {
                    services.AddSingleton<IDataContext, DataContext>();
                })
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    if (!InsideIIS())
                    {
                        webBuilder.UseKestrel();
                    }
                    else
                    {
                        webBuilder.UseIIS();
                    }

                    webBuilder.UseContentRoot(Directory.GetCurrentDirectory());
                    webBuilder.UseStartup<Startup>();
                });

            if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
            {
                host.UseWindowsService();
            }

            return host;
        }

    }
}