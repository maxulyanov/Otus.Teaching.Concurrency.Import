﻿
using MultithreadProjectCLI.Generator;
using MultithreadProjectCLI.Math;
using System.Diagnostics;

/// <summary>
/// Sequence length constant
/// </summary>
const int SEQUENCE_COUNT = 100000;

/// <summary>
/// Elapsed time measure function
/// </summary>
Action<int, Action> measure = (int iterationsCnt, Action func) =>
{
    Stopwatch sw = new();
    for (int i = 0; i < iterationsCnt; i++)
    {
        sw.Restart();
        func();
        Console.WriteLine($"Execution time: {sw.ElapsedMilliseconds}ms");
    }
};

/// <summary>
/// Sequence generator object
/// </summary>
IntGenerator intGenerator = new();

/// <summary>
/// Sequence of integer, global store variable
/// </summary>
List<int> intSequence = new();

Console.WriteLine($"{SEQUENCE_COUNT} integer sequence generation: ");
measure(1, () => intSequence = intGenerator.SequenceGenerate(SEQUENCE_COUNT).ToList());

Console.WriteLine($"1. Sequence sum ({nameof(SequenceMath.SumOneThreadForLoop)}): ");
measure(1, () => Console.WriteLine($"Total: {SequenceMath.SumOneThreadForLoop(intSequence.ToArray())}"));

Console.WriteLine($"2. Sequence sum ({nameof(SequenceMath.SumOneThreadLinq)}): ");
measure(1, () => Console.WriteLine($"Total: {SequenceMath.SumOneThreadLinq(intSequence)}"));

Console.WriteLine($"3. Sequence sum ({nameof(SequenceMath.SumThreadParallelAsync)}): ");
measure(1, () => Console.WriteLine($"Total: { SequenceMath.SumThreadParallelAsync(intSequence).Result}"));

Console.WriteLine($"4. Sequence sum ({nameof(SequenceMath.SumLinqParallelAsync)}): ");
measure(1, () => Console.WriteLine($"Total: { SequenceMath.SumLinqParallelAsync(intSequence)}"));

Console.WriteLine("Exit");
