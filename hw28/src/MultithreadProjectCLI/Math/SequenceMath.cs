﻿using System.Collections.Concurrent;

namespace MultithreadProjectCLI.Math
{
    /// <summary>
    /// Math helper class for different calculations of sequence elements
    /// </summary>
    internal static class SequenceMath
    {
        /// <summary>
        /// For loop sum
        /// </summary>
        /// <param name="sequence"></param>
        /// <returns></returns>
        internal static long SumOneThreadForLoop(int[] sequence)
        {
            long result = 0;
            for (int i = 0; i < sequence.Length; i++)
            {
                result += sequence[i];
            }

            return result;
        }

        /// <summary>
        /// Linq sum 
        /// </summary>
        /// <param name="sequence"></param>
        /// <exception cref="OverflowException"/>
        /// <returns></returns>
        internal static long SumOneThreadLinq(List<int> sequence) => sequence.Sum();

        /// <summary>
        /// Thread pool sum
        /// </summary>
        /// <param name="sequence"></param>
        /// <returns></returns>
        internal static async Task<long> SumThreadParallelAsync(List<int> sequence)
        {
            long result = 0;
            var chunks = sequence.Chunk(Environment.ProcessorCount);
            List<Task> tasks = new();

            foreach (var chunk in chunks)
                tasks.Add(Task.Run(() => {
                    long sum = SumOneThreadForLoop(chunk);
                    Interlocked.Add(ref result, sum); 
                }));

            await Task.WhenAll(tasks);

            return result;
        }

        /// <summary>
        /// Parallel linq sum
        /// </summary>
        /// <param name="sequence"></param>
        /// <returns></returns>
        internal static long SumLinqParallelAsync(List<int> sequence)
        {
            ConcurrentBag<long> resultBag = new();
            sequence.Chunk(Environment.ProcessorCount).AsParallel().ForAll(task => resultBag.Add(SumOneThreadForLoop(task)));

            return resultBag.Sum();
        }
    }
}
