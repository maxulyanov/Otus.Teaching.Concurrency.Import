﻿namespace MultithreadProjectCLI.Generator
{
    /// <summary>
    /// Interface for auto generating an any object kind
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface ITypeGenerator<T>
    {
        IEnumerable<T> SequenceGenerate(int count);
        T Generate();
    }
}
