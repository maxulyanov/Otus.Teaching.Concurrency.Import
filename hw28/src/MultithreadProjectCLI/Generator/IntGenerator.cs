﻿namespace MultithreadProjectCLI.Generator
{
    /// <summary>
    /// Class for int sequence generation
    /// </summary>
    class IntGenerator : ITypeGenerator<int>
    {
        readonly Random _random = new();

        public int Generate() => _random.Next(-10,10);
        public IEnumerable<int> SequenceGenerate(int count) => Enumerable.Range(0, count).Select(i => Generate());
    }
}
