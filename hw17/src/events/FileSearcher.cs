﻿namespace events
{
    /// <summary>
    /// Интерфейс объекта для поиска файлов
    /// </summary>
    public interface IFileSearcher : IDisposable
    {
        /// <summary>
        /// Событие найденного файла
        /// </summary>
        event EventHandler FileFound;

        /// <summary>
        /// Асинхронный метод поиска файлов
        /// </summary>
        /// <param name="directory">путь к директории поиска</param>
        /// <param name="searchPattern">паттерн поиска, если не указан ищутся любые файлы</param>
        /// <returns></returns>
        Task RunSearchAsync(string directory, string searchPattern = "*");

        /// <summary>
        /// Метод поиска файлов
        /// </summary>
        /// <param name="directory">путь к директории поиска</param>
        /// <param name="searchPattern">паттерн поиска, если не указан ищутся любые файлы</param>
        /// <returns></returns>
        void RunSearch(string directory, string searchPattern = "*");

        /// <summary>
        /// Остановить поиск файлов
        /// </summary>
        void StopSearch();

        /// <summary>
        /// Метод очищает все подписки на событие FileFound
        /// </summary>
        void RemoveAllFoundEvents();

        /// <summary>
        /// Флаг активного поиска
        /// </summary>
        public bool isRunning { get; }
    }

    /// <summary>
    /// Класс методов поиска файлов
    /// </summary>
    public class FileSearcher : IFileSearcher
    {
        private CancellationTokenSource? _source;
        readonly List<EventHandler> _delegates = new();

#pragma warning disable CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.
        private event EventHandler _fileFound;
        public event EventHandler FileFound
        {
            add
            {
                _fileFound += value;
                _delegates.Add(value);
            }

            remove
            {
                _fileFound -= value;
                _delegates.Remove(value);
            }
        }
#pragma warning restore CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.

        public bool isRunning { get; private set; }

        /// <summary>
        /// Вспомогательный метод асинхронной итерации и поиску файлов
        /// </summary>
        /// <param name="directory"></param>
        /// <param name="searchPattern"></param> 
        /// <returns></returns>
        private async Task SearchingAsync(string directory, string searchPattern)
        {
            using var e = await Task.Run(() => Directory.EnumerateFiles(directory, searchPattern, SearchOption.AllDirectories).GetEnumerator(), _source.Token);
            while (await Task.Run(() =>
            {
                try
                {
                    return e.MoveNext();
                }
                catch { /**/ }
                return false;
            }, _source.Token) && !_source.Token.IsCancellationRequested)
            {
                _fileFound?.Invoke(this, new FileArgs(e.Current));
            }
        }

        public async Task RunSearchAsync(string directory, string searchPattern = "*")
        {
            // останавливаем предыдущий поиск если был запущен
            StopSearch();

            _source = new CancellationTokenSource();

            if (Directory.Exists(directory))
            {
                isRunning = true;
                try
                {
                    await SearchingAsync(directory, searchPattern);
                }
                finally
                {
                    isRunning = false;
                }
            }
        }

        public void RunSearch(string directory, string searchPattern = "*")
        {
            // останавливаем предыдущий поиск если был запущен
            StopSearch();

            _source = new CancellationTokenSource();
            if (Directory.Exists(directory))
            {
                isRunning = true;
                try
                {
                    SearchingAsync(directory, searchPattern).Wait(_source.Token);
                }
                finally
                {
                    isRunning = false;
                }
            }
        }

        public void StopSearch()
        {
            if (_source != null && !_source.IsCancellationRequested)
            {
                _source.Cancel();
            }

            _source?.Dispose();
        }

        public void RemoveAllFoundEvents()
        {
            foreach (EventHandler eh in _delegates)
            {
                _fileFound -= eh;
            }
            _delegates.Clear();
        }

        #region Dispose

        private bool _disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed && disposing)
            {
                StopSearch();
                RemoveAllFoundEvents();

                _disposed = true;
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion
    }
}
