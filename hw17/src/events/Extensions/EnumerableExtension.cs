﻿namespace events.Extensions
{
    public static class EnumerableExtension
    {
        public static T? GetMax<T>(this IEnumerable<T> collection, Func<T, float> getParameter) where T : class
        {
            if (getParameter == null)
                return default;

            float? maxFloat = null;
            T? max = null;

            foreach (T e in collection)
            {
                var result = getParameter.Invoke(e);
                if (maxFloat == null || maxFloat < result)
                {
                    maxFloat = result;
                    max = e;
                }
            };

            return max;
        }
    }
}
