﻿namespace events
{
    /// <summary>
    /// Класс презентации параметров для события найденного файла
    /// </summary>
    public class FileArgs : EventArgs
    {
        private string _path;
        public string FilePath => _path;
        public string FileName => Path.GetFileName(FilePath ?? string.Empty);

        public FileArgs(string path)
        {
            _path = path;
        }
    }
}
