﻿namespace ProrotypePattern
{
    /// <summary>
    /// Дженерик интерфейс для описания методов паттерна прототип
    /// </summary>
    /// <typeparam name="T">Тип объекта клонирования</typeparam>
    public interface IMyClonable<T>
    {
        T Replica();
    }
}
