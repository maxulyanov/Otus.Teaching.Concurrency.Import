﻿using System.Text;

namespace ProrotypePattern
{
    /// <summary>
    /// Класс описания зоопарка
    /// </summary>
    public class Zoo : IMyClonable<Zoo>, ICloneable
    {
        /// <summary> Список обитателей зоопарка </summary>
        private readonly List<AnimalPrototype> _animals;

        public string Name { get; private set; }
        public string Address { get; private set; }
        public int BirdsCount => _animals.Where(w => w is Bird).Count();
        public int BearsCount => _animals.Where(w => w is Bear).Count();

        public Zoo(string name, string address)
        {
            _animals = new List<AnimalPrototype>();
            Name = name;
            Address = address;
        }

        /// <summary>
        /// Метод изменения названия зоопарка
        /// </summary>
        /// <param name="name"></param>
        public void SetName(string name)
        {
            Name = name;
        }

        /// <summary>
        /// Метод добавления животного в зоопарк
        /// </summary>
        /// <param name="animal"></param>
        public void AddAnimal(AnimalPrototype animal)
        {
            _animals.Add(animal);
        }

        /// <summary>
        /// Индексер для поиска животного по имени
        /// </summary>
        /// <param name="name">название животного</param>
        /// <returns></returns>
        public AnimalPrototype? this[string name] => _animals.FirstOrDefault(w => (w is Bird) && w.Name == name);

        public override string ToString()
        {
            StringBuilder sb = new();
            sb.AppendLine($"{Name}, address {Address}, birds count:{BirdsCount}, bears count: {BearsCount}");
            _animals.ForEach(w => sb.AppendLine(w.ToString()));
            return sb.ToString();
        }

        public Zoo Replica()
        {
            // shallow copy
            Zoo copy = new(Name, Address);
            // deep copy
            _animals.ForEach((animal) => copy.AddAnimal(animal.Replica()));

            return copy;
        }

        public object Clone() => Replica();
    }
}
