﻿namespace ProrotypePattern
{
    /// <summary>
    /// Класс описания медведей
    /// </summary>
    public class Bear : AnimalPrototype
    {
        /// <summary> Вес медведя </summary>
        public int Weight { get; private set; }
        /// <summary> Рацион медведя </summary>
        public string Diet { get; private set; }

        public Bear(string name, int weight, string diet) : base(name)
        {
            Weight = weight;
            Diet = diet;
        }

        public void SetWeight(int weight)
        {
            Weight = weight;
        }

        public void SetDiet(string diet)
        {
            Diet = diet;
        }

        public override AnimalPrototype Replica()
        {
            return (Bear)MemberwiseClone();
        }

        public override string ToString()
        {
            return $"Bear '{Name}' has a weight {Weight}kg and eats {Diet}";
        }
    }
}
