﻿namespace ProrotypePattern
{
    /// <summary>
    /// Абстрактный класс какого-либо животного
    /// </summary>
    public abstract class AnimalPrototype : IMyClonable<AnimalPrototype>, ICloneable
    {
        /// <summary> Имя животного </summary>
        public string Name { get; private set; }
        public AnimalPrototype(string name)
        {
            Name = name;
        }        

        public void SetName(string name)
        {
            Name = name;
        }

        public abstract AnimalPrototype Replica();
        public abstract override string ToString();

        /// <summary>
        /// IClonable interface implementation
        /// </summary>
        /// <returns></returns>
        public virtual object Clone() => Replica();
    }
}
