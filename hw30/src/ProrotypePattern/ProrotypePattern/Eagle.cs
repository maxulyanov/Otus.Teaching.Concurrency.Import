﻿namespace ProrotypePattern
{
    public class Eagle : Bird
    {
        public Eagle(string name, int flyHeight, int flyRange, int flySpeed) : base(name, flyHeight, flyRange, flySpeed)
        {
        }

        public Eagle(Bird bird) : base(bird.Name, bird.FlyHeight, bird.FlyRange, bird.FlySpeed)
        {
        }

        public override AnimalPrototype Replica()
        {
            return (Eagle)MemberwiseClone();
        }

        public override string ToString()
        {
            return $"Eagle '{Name}' flying at altitude {FlyHeight}m in range {FlyRange}km with speed {FlySpeed}km/hr";
        }
    }
}
