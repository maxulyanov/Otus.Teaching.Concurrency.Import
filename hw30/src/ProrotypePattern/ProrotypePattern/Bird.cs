﻿namespace ProrotypePattern
{
    /// <summary>
    /// Класс описания птиц
    /// </summary>
    public class Bird : AnimalPrototype
    {
        /// <summary> Высота полёта в метрах </summary>
        public int FlyHeight { get; private set; }
        /// <summary> Дальность полёта </summary>
        public int FlyRange { get; private set; }
        /// <summary> Скорость полёта в км/ч </summary>
        public int FlySpeed { get; private set; }

        public Bird(string name, int flyHeight, int flyRange, int flySpeed) : base(name)
        {
            FlyHeight = flyHeight;
            FlyRange = flyRange;
            FlySpeed = flySpeed;
        }

        public void SetFlySpeed(int flySpeed)
        {
            FlySpeed = flySpeed;
        }

        public void SetFlyHeight(int flyHeight)
        {
            FlyHeight = flyHeight;
        }

        public void SetFlyRange(int flyRange)
        {
            FlyRange = flyRange;
        }

        public override AnimalPrototype Replica()
        {
            return (Bird)MemberwiseClone();
        }

        public override string ToString()
        {
            return $"Bird '{Name}' flying at altitude {FlyHeight}m in range {FlyRange}km with speed {FlySpeed}km/hr";
        }
    }
}
