﻿namespace ProrotypePattern
{
    public class PolarBear : Bear
    {
        public PolarBear(string name, int weight, string diet) : base(name, weight, diet)
        {
        }

        public override AnimalPrototype Replica()
        {
            return (PolarBear)MemberwiseClone();
        }

        public override string ToString()
        {
            return $"Polar bear '{Name}' has a weight {Weight}kg and eats {Diet}";
        }
    }
}
