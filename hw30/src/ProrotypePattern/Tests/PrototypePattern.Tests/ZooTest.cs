using ProrotypePattern;
using System.Diagnostics;
using Xunit;

namespace PrototypePattern.Tests
{
    public class ZooTest
    {
        private Zoo _zoo;

        public ZooTest()
        {
            _zoo = new Zoo("Central Park Zoo", "East 64th Street, New York, NY 10021, United States");
        }

        [Fact]
        public void AppendClonedBirds()
        {
            var bird1 = new Bird("Eagle Stepasha", 1000, 10000, 120);
            var bird2 = bird1.Replica();
            bird2.SetName("Eagle Masha");
            var bird3 = new Eagle(bird2 as Bird);

            Assert.NotSame(bird1, bird2);

            _zoo.AddAnimal(bird1);
            _zoo.AddAnimal(bird2);
            _zoo.AddAnimal(bird3);

            Debug.Write(_zoo.ToString());

            Assert.Equal(3, _zoo.BirdsCount);
            Assert.Equal(0, _zoo.BearsCount);

            Assert.NotEqual(bird1.Name, bird2.Name);
        }

        [Fact]
        public void AppendClonedBears()
        {
            var bear1 = new Bear("Bear Potap", 1000, "Fish and chips");
            var bear2 = bear1.Replica();
            bear2.SetName("Bear Wild");

            Assert.NotSame(bear1, bear2);

            _zoo.AddAnimal(bear1);
            _zoo.AddAnimal(bear2);

            Debug.Write(_zoo.ToString());

            Assert.Equal(2, _zoo.BearsCount);
            Assert.Equal(0, _zoo.BirdsCount);

            Assert.NotEqual(bear1.Name, bear2.Name);
        }

        [Fact]
        public void CloneEntireZoo()
        {
            var bird1 = new Bird("Eagle Stepasha", 1000, 10000, 120);
            var bear1 = new Bear("Bear Potap", 1000, "Fish and chips");
            var bear2 = new PolarBear("Bear White", 1000, "Fish and Seal");

            _zoo.AddAnimal(bear1);
            _zoo.AddAnimal(bird1);
            _zoo.AddAnimal(bear2);

            Assert.Equal(2, _zoo.BearsCount);
            Assert.Equal(1, _zoo.BirdsCount);

            Zoo anotherZoo = (Zoo)_zoo.Clone();

            Assert.Equal(_zoo.BearsCount, anotherZoo.BearsCount);

            var bear3 = bear2.Replica();
            bear3.SetName("Bear Wild");
            anotherZoo.AddAnimal(bear3);

            Assert.NotEqual(_zoo.BearsCount, anotherZoo.BearsCount);
            Assert.NotSame(_zoo, anotherZoo);

            Debug.Write(anotherZoo.ToString());
        }
    }
}