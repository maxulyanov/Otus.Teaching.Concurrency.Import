﻿using CsvFormater;
using Newtonsoft.Json;
using Reflection.Models;
using System.Diagnostics;
using System.Text.Json;

Action<int,Action> measure = (int iterationsCnt, Action func) => {
    Stopwatch sw = Stopwatch.StartNew();
    for (int i = 0; i < iterationsCnt; i++)
    {
        func();
    }
    Console.WriteLine(sw.ElapsedMilliseconds);
};



F csvObj = F.Get();
string csvStr = csvObj.SerializeToCsv();
string jsonString = System.Text.Json.JsonSerializer.Serialize(csvObj);
int iterations = 10000;
Console.WriteLine($"CSV сериализация/десериализация измерение, {iterations} итераций");

Console.Write("\tВремя на сериализацию CSV мой метод (мс): ");
measure(iterations, () =>
    csvObj.SerializeToCsv()
);

Console.Write("\tВремя на сериализацию System.Text.Json (мс): ");
measure(iterations, () =>
    System.Text.Json.JsonSerializer.Serialize(csvObj)
);

Console.Write("\tВремя на сериализацию Newtonsoft.Json (мс): ");
measure(iterations, () =>
    JsonConvert.SerializeObject(csvObj)
);

Console.Write("\tВремя на десериализацию CSV мой метод (мс): ");
measure(iterations, () =>
    csvStr.DeserializeFromCsv<F>()
);

Console.Write("\tВремя на десериализацию System.Text.Json (мс): ");
measure(iterations, () =>
    System.Text.Json.JsonSerializer.Deserialize<F>(jsonString)
);

Console.Write("\tВремя на десериализацию Newtonsoft.Json (мс): ");
measure(iterations, () =>
    JsonConvert.DeserializeObject<F>(jsonString)
);

Console.WriteLine($"Завершено");