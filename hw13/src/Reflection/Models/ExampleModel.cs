﻿namespace Reflection.Models
{
    /// <summary>
    /// Класс данных для теста расширенной поддержки форматов сериализации
    /// </summary>
    public class ExampleModel
    {
        public string? Name { get; set; }
        public string? Description { get; set; }
        public bool IsDeleted { get; set; }
        public int Age { get; set; }
        public byte[] Data { get; set; }
        public float Heigth { get; set; }

        public static ExampleModel Get() => new() { Age = 12, Heigth = 0.14f, Name = "Name", Data = new byte[0], Description = "Description", IsDeleted = true };
    }
}
